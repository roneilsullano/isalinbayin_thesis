import { Component } from '@angular/core';
import { NavController, ActionSheetController } from 'ionic-angular';
import { Camera, PictureSourceType } from '@ionic-native/camera';
import * as Tesseract from 'tesseract.js'
import { NgProgress } from '@ngx-progressbar/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html'
})
export class ScanPage {

  selectedImage: string;
  imageText: any;
  english: any;
  filipino: any;
  raw:any;
  baybayin:any;
  words:any;
  baybayinStatus = false;

  constructor(public navCtrl: NavController, private camera: Camera, private actionSheetCtrl: ActionSheetController, public progress: NgProgress, public http: Http) {
  }

  selectSource() {
    this.english= '';
    this.baybayin='';
    this.filipino='';
    this.baybayinStatus = false;
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Get in Gallery',
          handler: () => {
            this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        }, {
          text: 'Capture Image',
          handler: () => {
            this.getPicture(this.camera.PictureSourceType.CAMERA);
          }
        }, {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  getPicture(sourceType: PictureSourceType) {
    this.camera.getPicture({
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: sourceType,
      allowEdit: true,
      saveToPhotoAlbum: false,
      correctOrientation: true
    }).then((imageData) => {
      this.selectedImage = `data:image/jpeg;base64,${imageData}`;
    });
  }

  recognizeImage() {
    this.english= '';
    this.filipino='';
    this.baybayin='';
    this.baybayinStatus = false;
    this.imageText = "Reading Text";
    Tesseract.recognize(this.selectedImage)
      .progress(message => {
        if (message.status === 'processing text')
          this.progress.set(message.progress);
      })
      .catch(err => console.error(err))
      .then(result => {
        this.imageText='';
        console.log(result);
        for (let index = 0; index < result.lines.length; index++) {
      
          this.imageText=this.imageText+result.lines[index].text+' ';
        }
        console.log(this.imageText);
        this.convertext(this.imageText);
      })
      .finally(resultOrError => {
        this.progress.complete();
      });
  
  }
  convertext(data) {
    console.log(data);
    this.http.get("https://translate.googleapis.com/translate_a/single?client=gtx&sl=tl&tl=en&dt=t&q=" + data)
      .map(res => res.json()).subscribe(data => {
        for (let index = 0; index < data[0].length; index++) {
          this.english = this.english + data[0][index][0]
          console.log( this.english)
        }

      });
    this.http.get("https://translate.googleapis.com/translate_a/single?client=gtx&sl=en&tl=tl&dt=t&q=" + data)
      .map(res => res.json()).subscribe(data => {
        for (let index = 0; index < data[0].length; index++) {
          this.filipino = this.filipino + data[0][index][0];
          this.raw = this.filipino;
          console.log(this.filipino);
          this.processBaybay();
          
        }
      });
  }
 
  processBaybay() {
    this.baybayinStatus = false;
		this.baybayin = '';
		this.raw = this.raw.toLowerCase();
		this.words = this.raw.split(" ");
		this.words.forEach(element => {
			var currentstring = element.length;
			if (element.length > 2) {
				for (let index = element.length - 2; index > 0; index--) {
					if (element.charAt(index+1) == '='|| element.charAt(index) == '=') {
						continue;
					}
					if (this.vowelbool(element.charAt(index)) == false && this.vowelbool(element.charAt(index + 1)) == true) {
						continue;
					}
					if (this.vowelbool(element.charAt(index)) == true && this.vowelbool(element.charAt(index - 1)) == true) {
						element = this.setCharAt(element, index, element.charAt(index).toUpperCase());
						continue;
					}
					if (this.vowelbool(element.charAt(index)) == false && this.vowelbool(element.charAt(index - 1)) == false && this.vowelbool(element.charAt(index + 1)) == false && element.search('ng') < 0) {
						element = this.setCharAt(element, index, element.charAt(index) + '=');
						
						continue;
					}
					else if (this.vowelbool(element.charAt(index)) == false && this.vowelbool(element.charAt(index - 1)) == true && this.vowelbool(element.charAt(index + 1)) == false && element.search('ng') < 0) {
						element = this.setCharAt(element, index, element.charAt(index) + '=');					
						continue;
					}				
				}
			}
			 currentstring = element.length;
			if (this.vowelbool(element.charAt(currentstring - 1)) == false && element.charAt(currentstring - 1) != '=') {
				element = this.setCharAt(element, currentstring - 1, element.charAt(currentstring - 1) + '=');
			}
			if (this.vowelbool(element.charAt(currentstring - 1)) == true && this.vowelbool(element.charAt(currentstring - 2)) == true) {
				element = this.setCharAt(element, currentstring - 1, element.charAt(currentstring - 1).toUpperCase());
			}
			if (this.vowelbool(element.charAt(0))) {
				element = this.setCharAt(element, 0, element.charAt(0).toUpperCase());
			}
			if (this.vowelbool(element.charAt(0)) == false && this.vowelbool(element.charAt(1)) == false && (element.charAt(1) != 'n' && element.charAt(1) != 'g')) {
				element = this.setCharAt(element, 0, element.charAt(0) + '=');
			}
			var element = element.replace('ng', 'N');
			this.baybayin = this.baybayin+ element +' ';
      this.baybayinStatus = true;
		});
		
		
	}
	setCharAt(str, index, chr) {
		if (index > str.length - 1) return str;
		return str.substr(0, index) + chr + str.substr(index + 1);
	}
	vowelbool(char) {
		return /^[aeiou=]$/.test(char.toLowerCase());
	}

}
