import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-keyboard',
  templateUrl: 'keyboard.html'
})
export class KeyboardPage {
  filipino = '';
  baybayin = '';
  words:any;
alphabet = [
  {title:'A',sub:'a', class:''},
  {title:'E',sub:'e', class:''},
  {title:'I',sub:'i', class:''},
  {title:'O',sub:'o', class:''},
  {title:'U',sub:'u', class:''},
  {title:'b=',sub:'b', class:''},
  {title:'k=',sub:'k', class:''},
  {title:'d=',sub:'d', class:''},
  {title:'g=',sub:'g', class:''},
  {title:'h=',sub:'h', class:''},
  {title:'l=',sub:'l', class:''},
  {title:'m=',sub:'m', class:''},
  {title:'n=',sub:'n', class:'lowercase'},
  {title:'N=',sub:'ng', class:''},
  {title:'r=',sub:'r', class:''},
  {title:'p=',sub:'p', class:''},
  {title:'s=',sub:'s', class:''},
  {title:'t=',sub:'t', class:''},
  {title:'w=',sub:'w', class:''},
  {title:'y=',sub:'y', class:''},
  
 
];
  constructor(public navCtrl: NavController
   
 ) {
    
  }
  press(data){
  this.filipino = this.filipino + data.sub;
  // this.baybayin = this.baybayin + data.title;
  
  if(this.vowelbool(data.sub)&& this.baybayin.length>0&& this.baybayin.charAt(this.baybayin.length-1) =='='){

  // this.setCharAt(this.baybayin, this.baybayin.length-1,'');
  this.baybayin = this.baybayin.substring(0, this.baybayin.length - 1);
  this.baybayin = this.baybayin + data.title.toLowerCase();
  // this.baybayin = this.baybayin.substring(0, this.baybayin.length - 1);
  }
  else{
    this.baybayin = this.baybayin + data.title;
  }
console.log(this.baybayin);
    

	}
remove(){
  if(this.filipino.length==0||this.filipino.length==1){
    this.filipino = '';
    this.baybayin =  '';
  }
  else if (this.filipino.charAt(this.filipino.length-1) =='g'&&this.filipino.charAt(this.filipino.length-2) =='n') {
    this.baybayin = this.baybayin.substring(0, this.baybayin.length - 1);
    this.baybayin = this.baybayin.substring(0, this.baybayin.length - 1);
    this.filipino = this.filipino.substring(0, this.filipino.length - 1);
    this.filipino = this.filipino.substring(0, this.filipino.length - 1);
  }
  else if(this.filipino.charAt(this.filipino.length-1) ==' '){
    
    this.baybayin = this.baybayin.substring(0, this.baybayin.length - 1);
    this.filipino = this.filipino.substring(0, this.filipino.length - 1);
  }
  
  else if (this.vowelbool(this.filipino.charAt(this.filipino.length-1))) { 
    this.filipino = this.filipino.substring(0, this.filipino.length - 1);
    this.baybayin = this.baybayin.substring(0, this.baybayin.length - 1);
    if (this.vowelbool(this.filipino.charAt(this.filipino.length-1))) {
      this.baybayin.charAt(this.filipino.length-1).toUpperCase();
    }else if(this.filipino.charAt(this.filipino.length-1) ==' '||this.filipino.charAt(this.filipino.length-1) =='='){
    
    } else if(this.vowelbool(this.filipino.charAt(this.filipino.length-1))==false){
      this.baybayin = this.baybayin + '=';
    }
  }else if(this.vowelbool(this.filipino.charAt(this.filipino.length-1))==false){
    this.baybayin = this.baybayin.substring(0, this.baybayin.length - 1);
    this.baybayin = this.baybayin.substring(0, this.baybayin.length - 1);
    this.filipino = this.filipino.substring(0, this.filipino.length - 1);
    console.log(this.baybayin);
    if (this.vowelbool(this.filipino.charAt(this.filipino.length-1))) {
      this.baybayin.charAt(this.filipino.length-1).toUpperCase();
    }
  }
  console.log(this.baybayin);
  console.log(this.filipino);

  
  
}
reset(){
  this.filipino = '';
    this.baybayin =  '';
}
space(){
  this.filipino = this.filipino + ' ';
   this.baybayin = this.baybayin +' ';
}

	setCharAt(str, index, chr) {
		if (index > str.length - 1) return str;
		return str.substr(0, index) + chr + str.substr(index + 1);
	}
	vowelbool(char) {
		return /^[aeiou=]$/.test(char.toLowerCase());
	}

}
