import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
// import { e } from '@angular/core/src/render3';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
	selector: 'page-form',
	templateUrl: 'form.html'
})
export class FormPage {
	raw: any;
	words: any;
	english = '';
	filipino = '';
	reserve: any;
	consonant = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'];
	vowel = ['a', 'e', 'i', 'o', 'u'];
	englishText: any;
	baybayinText: any;
	filipinoText: any;
	engstatus = false;
	filstatus = false;
	baystatus = false;

	navController: any;
	constructor(public navCtrl: NavController, public http: Http) {

		this.navController = navCtrl;
		// this.translationservice = translationservice;
	}
	status(data) {
		if (data == 'filipino') {
			this.englishText = '';
			this.baybayinText = '';
			this.engstatus = false;
			this.filstatus = true;
			this.baystatus = false;
		} else if (data == 'english') {
			this.filipinoText = '';
			this.baybayinText = '';
			this.engstatus = true;
			this.filstatus = false;
			this.baystatus = false;
		} else if (data == 'baybayin') {
			console.log(this.baybayinText);
			this.raw = this.baybayinText;
			this.reserve = this.raw;
			// this.baybayinText='';
			// this.processBaybay();
			this.filipinoText = '';
			this.englishText = '';
			this.engstatus = false;
			this.filstatus = false;
			this.baystatus = true;
		}

	}
	translate() {
		this.filipino = '';
		this.english = '';
		if (this.engstatus) {

			this.http.get("https://translate.googleapis.com/translate_a/single?client=gtx&sl=en&tl=tl&dt=t&q=" + this.englishText)
				.map(res => res.json()).subscribe(data => {
					console.log(data);
					console.log(data[0].length);
					for (let index = 0; index < data[0].length; index++) {
						this.filipino = this.filipino + data[0][index][0]

					}
					this.filipinoText = this.filipino;
					this.raw = this.filipinoText;
					this.processBaybay();
				});
		} else if (this.filstatus) {
			this.raw = this.filipinoText;
			this.http.get("https://translate.googleapis.com/translate_a/single?client=gtx&sl=tl&tl=en&dt=t&q=" + this.raw)
				.map(res => res.json()).subscribe(data => {
					console.log(data);
				
					for (let index = 0; index < data[0].length; index++) {
						this.english = this.english + data[0][index][0]

					}
					this.englishText = this.english;
				});
			this.processBaybay();
		}
		else if (this.baystatus) {
			this.raw = this.reserve
			this.filipinoText = this.raw;
			this.http.get("https://translate.googleapis.com/translate_a/single?client=gtx&sl=tl&tl=en&dt=t&q=" + this.raw)
				.map(res => res.json()).subscribe(data => {
					console.log(data);
					console.log(data[0].length);

					for (let index = 0; index < data[0].length; index++) {
						this.english = this.english + data[0][index][0]

					}
					this.englishText = this.english;
				});

			this.processBaybay();
		}

	}
	processBaybay() {
		this.baybayinText = '';
		this.raw = this.raw.toLowerCase();

		this.words = this.raw.split(" ");

		console.log(this.words);
		this.words.forEach(element => {
			var currentstring = element.length;

			
			if (element.length > 2) {
			
				for (let index = element.length - 2; index > 0; index--) {
					console.log(element, index);
					if (element.charAt(index+1) == '='|| element.charAt(index) == '=') {
						continue;
					}
					if (this.vowelbool(element.charAt(index)) == false && this.vowelbool(element.charAt(index + 1)) == true) {
						console.log(element.charAt(index), element.charAt(index + 1))
						continue;
					}


					if (this.vowelbool(element.charAt(index)) == true && this.vowelbool(element.charAt(index - 1)) == true) {
						element = this.setCharAt(element, index, element.charAt(index).toUpperCase());
						continue;
					}
					
					if (this.vowelbool(element.charAt(index)) == false && this.vowelbool(element.charAt(index - 1)) == false && this.vowelbool(element.charAt(index + 1)) == false && element.search('ng') < 0) {
						console.log(element.charAt(index), element.charAt(index + 1))
						element = this.setCharAt(element, index, element.charAt(index) + '=');
						
						continue;
					}
					else if (this.vowelbool(element.charAt(index)) == false && this.vowelbool(element.charAt(index - 1)) == true && this.vowelbool(element.charAt(index + 1)) == false && element.search('ng') < 0) {
						console.log(element.charAt(index), element.charAt(index + 1))
						element = this.setCharAt(element, index, element.charAt(index) + '=');
						
						continue;
					}
					
				}
				
			
				console.log(element);
				
			}
			 currentstring = element.length;
			if (this.vowelbool(element.charAt(currentstring - 1)) == false && element.charAt(currentstring - 1) != '=') {
				
				element = this.setCharAt(element, currentstring - 1, element.charAt(currentstring - 1) + '=');
			
			}
			if (this.vowelbool(element.charAt(currentstring - 1)) == true && this.vowelbool(element.charAt(currentstring - 2)) == true) {
				element = this.setCharAt(element, currentstring - 1, element.charAt(currentstring - 1).toUpperCase());
			}
			if (this.vowelbool(element.charAt(0))) {
				element = this.setCharAt(element, 0, element.charAt(0).toUpperCase());
			}
			if (this.vowelbool(element.charAt(0)) == false && this.vowelbool(element.charAt(1)) == false && (element.charAt(1) != 'n' && element.charAt(1) != 'g')) {
				console.log(element.charAt(0), element.charAt(1))
				element = this.setCharAt(element, 0, element.charAt(0) + '=');
				
			}
			var element = element.replace('ng', 'N');
			console.log(element)
			this.baybayinText = this.baybayinText + element + ' '
		});
		
		console.log(this.baybayinText);
	}
	setCharAt(str, index, chr) {
		if (index > str.length - 1) return str;
		return str.substr(0, index) + chr + str.substr(index + 1);
	}
	vowelbool(char) {
		return /^[aeiou=]$/.test(char.toLowerCase());
	}




}
