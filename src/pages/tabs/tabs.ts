import { Component } from '@angular/core';

import { ScanPage } from '../scan/scan';
import { FormPage } from '../form/form';
import { HomePage } from '../home/home';
import { KeyboardPage } from '../keyboard/keyboard';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ScanPage;
  tab3Root = FormPage;
  tab4Root = KeyboardPage;
  constructor() {

  }
}
