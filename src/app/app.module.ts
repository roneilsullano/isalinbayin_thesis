import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Camera } from '@ionic-native/camera';
import { NgProgressModule } from '@ngx-progressbar/core';

import { ScanPage } from '../pages/scan/scan';
import { FormPage } from '../pages/form/form';
import { HomePage } from '../pages/home/home';
import { KeyboardPage } from '../pages/keyboard/keyboard';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import {TranslationService}  from './providers/translation.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    MyApp,
    ScanPage,
    FormPage,
    HomePage,
    TabsPage,
    KeyboardPage
  ],
  imports: [
    HttpModule,
    HttpClientModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    NgProgressModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ScanPage,
    FormPage,
    HomePage,
    TabsPage,
    KeyboardPage
  ],
  providers: [
    
    StatusBar,
    SplashScreen,
    Camera,
  
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    
   
  ]
})
export class AppModule {}
