webpackJsonp([0],{

/***/ 114:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 114;

/***/ }),

/***/ 156:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 156;

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__scan_scan__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__form_form__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__keyboard_keyboard__ = __webpack_require__(212);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__scan_scan__["a" /* ScanPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__form_form__["a" /* FormPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_4__keyboard_keyboard__["a" /* KeyboardPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/roneil/Desktop/alibator/camscanner-seed/src/pages/tabs/tabs.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle="" tabIcon="home"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="" tabIcon="qr-scanner"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="" tabIcon="git-compare"></ion-tab>\n  <ion-tab [root]="tab4Root" tabTitle="" tabIcon="keypad"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/home/roneil/Desktop/alibator/camscanner-seed/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_tesseract_js__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_tesseract_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_tesseract_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_progressbar_core__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ScanPage = /** @class */ (function () {
    function ScanPage(navCtrl, camera, actionSheetCtrl, progress, http) {
        this.navCtrl = navCtrl;
        this.camera = camera;
        this.actionSheetCtrl = actionSheetCtrl;
        this.progress = progress;
        this.http = http;
        this.baybayinStatus = false;
    }
    ScanPage.prototype.selectSource = function () {
        var _this = this;
        this.english = '';
        this.baybayin = '';
        this.filipino = '';
        this.baybayinStatus = false;
        var actionSheet = this.actionSheetCtrl.create({
            buttons: [
                {
                    text: 'Get in Gallery',
                    handler: function () {
                        _this.getPicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                }, {
                    text: 'Capture Image',
                    handler: function () {
                        _this.getPicture(_this.camera.PictureSourceType.CAMERA);
                    }
                }, {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    ScanPage.prototype.getPicture = function (sourceType) {
        var _this = this;
        this.camera.getPicture({
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: sourceType,
            allowEdit: true,
            saveToPhotoAlbum: false,
            correctOrientation: true
        }).then(function (imageData) {
            _this.selectedImage = "data:image/jpeg;base64," + imageData;
        });
    };
    ScanPage.prototype.recognizeImage = function () {
        var _this = this;
        this.english = '';
        this.filipino = '';
        this.baybayin = '';
        this.baybayinStatus = false;
        this.imageText = "Reading Text";
        __WEBPACK_IMPORTED_MODULE_3_tesseract_js__["recognize"](this.selectedImage)
            .progress(function (message) {
            if (message.status === 'processing text')
                _this.progress.set(message.progress);
        })
            .catch(function (err) { return console.error(err); })
            .then(function (result) {
            _this.imageText = '';
            console.log(result);
            for (var index = 0; index < result.lines.length; index++) {
                _this.imageText = _this.imageText + result.lines[index].text + ' ';
            }
            // this.imageText = result.text;
            console.log(_this.imageText);
            _this.convertext(_this.imageText);
        })
            .finally(function (resultOrError) {
            _this.progress.complete();
        });
    };
    ScanPage.prototype.convertext = function (data) {
        var _this = this;
        console.log(data);
        this.http.get("https://translate.googleapis.com/translate_a/single?client=gtx&sl=tl&tl=en&dt=t&q=" + data)
            .map(function (res) { return res.json(); }).subscribe(function (data) {
            for (var index = 0; index < data[0].length; index++) {
                _this.english = _this.english + data[0][index][0];
                console.log(_this.english);
            }
        });
        this.http.get("https://translate.googleapis.com/translate_a/single?client=gtx&sl=en&tl=tl&dt=t&q=" + data)
            .map(function (res) { return res.json(); }).subscribe(function (data) {
            for (var index = 0; index < data[0].length; index++) {
                _this.filipino = _this.filipino + data[0][index][0];
                _this.raw = _this.filipino;
                console.log(_this.filipino);
                _this.processBaybay();
            }
        });
    };
    ScanPage.prototype.processBaybay = function () {
        var _this = this;
        this.baybayinStatus = false;
        this.baybayin = '';
        this.raw = this.raw.toLowerCase();
        this.words = this.raw.split(" ");
        this.words.forEach(function (element) {
            var currentstring = element.length;
            if (element.length > 2) {
                for (var index = element.length - 2; index > 0; index--) {
                    if (element.charAt(index + 1) == '=' || element.charAt(index) == '=') {
                        continue;
                    }
                    if (_this.vowelbool(element.charAt(index)) == false && _this.vowelbool(element.charAt(index + 1)) == true) {
                        continue;
                    }
                    if (_this.vowelbool(element.charAt(index)) == true && _this.vowelbool(element.charAt(index - 1)) == true) {
                        element = _this.setCharAt(element, index, element.charAt(index).toUpperCase());
                        continue;
                    }
                    if (_this.vowelbool(element.charAt(index)) == false && _this.vowelbool(element.charAt(index - 1)) == false && _this.vowelbool(element.charAt(index + 1)) == false && element.search('ng') < 0) {
                        element = _this.setCharAt(element, index, element.charAt(index) + '=');
                        continue;
                    }
                    else if (_this.vowelbool(element.charAt(index)) == false && _this.vowelbool(element.charAt(index - 1)) == true && _this.vowelbool(element.charAt(index + 1)) == false && element.search('ng') < 0) {
                        element = _this.setCharAt(element, index, element.charAt(index) + '=');
                        continue;
                    }
                }
            }
            currentstring = element.length;
            if (_this.vowelbool(element.charAt(currentstring - 1)) == false && element.charAt(currentstring - 1) != '=') {
                element = _this.setCharAt(element, currentstring - 1, element.charAt(currentstring - 1) + '=');
            }
            if (_this.vowelbool(element.charAt(currentstring - 1)) == true && _this.vowelbool(element.charAt(currentstring - 2)) == true) {
                element = _this.setCharAt(element, currentstring - 1, element.charAt(currentstring - 1).toUpperCase());
            }
            if (_this.vowelbool(element.charAt(0))) {
                element = _this.setCharAt(element, 0, element.charAt(0).toUpperCase());
            }
            if (_this.vowelbool(element.charAt(0)) == false && _this.vowelbool(element.charAt(1)) == false && (element.charAt(1) != 'n' && element.charAt(1) != 'g')) {
                element = _this.setCharAt(element, 0, element.charAt(0) + '=');
            }
            var element = element.replace('ng', 'N');
            console.log(element);
            _this.baybayin = _this.baybayin + element + ' ';
            _this.baybayinStatus = true;
        });
    };
    ScanPage.prototype.setCharAt = function (str, index, chr) {
        if (index > str.length - 1)
            return str;
        return str.substr(0, index) + chr + str.substr(index + 1);
    };
    ScanPage.prototype.vowelbool = function (char) {
        return /^[aeiou=]$/.test(char.toLowerCase());
    };
    ScanPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-scan',template:/*ion-inline-start:"/home/roneil/Desktop/alibator/camscanner-seed/src/pages/scan/scan.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      IsalinBayin\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ng-progress [min]="0" [max]="1" color="green"></ng-progress>\n\n  <button ion-button large full (click)="selectSource()">Scan Image</button>\n\n\n\n  <ion-card [ngStyle]="{ \'background-color\': \'#488AFF\' }" *ngIf="selectedImage">\n    <ion-card-header>\n      <div [ngStyle]="{\n        \'background-color\': \'#488AFF\',\n        color: \'#ffff99\',\n        \'font-	size\': \'14px\',\n        \'font-weight\': \'bold\',\n        float: \'left\'\n      }">\n        Image\n      </div>\n    </ion-card-header>\n    <ion-card-content>\n      <ion-list>\n        <ion-item>\n          <img [src]="selectedImage">\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n\n  <button ion-button large full (click)="recognizeImage()" *ngIf="selectedImage">Process Image</button>\n  <ion-card *ngIf="baybayinStatus">\n\n    <ion-card-header>\n      <div>\n        Baybayin\n      </div>\n    </ion-card-header>\n    <ion-card-content id="baybayin">\n      {{baybayin}}\n\n    </ion-card-content>\n  </ion-card>\n  <ion-card *ngIf="filipino">\n    <ion-card-header>\n      <div>\n        Filipino\n      </div>\n    </ion-card-header>\n    <ion-card-content>\n\n      {{filipino}}\n\n    </ion-card-content>\n  </ion-card>\n  <ion-card *ngIf="english">\n    <ion-card-header>\n      <div>\n        English\n      </div>\n    </ion-card-header>\n    <ion-card-content>\n\n      {{english}}\n\n    </ion-card-content>\n  </ion-card>\n  \n\n\n</ion-content>'/*ion-inline-end:"/home/roneil/Desktop/alibator/camscanner-seed/src/pages/scan/scan.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_4__ngx_progressbar_core__["a" /* NgProgress */], __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */]])
    ], ScanPage);
    return ScanPage;
}());

//# sourceMappingURL=scan.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { e } from '@angular/core/src/render3';


var FormPage = /** @class */ (function () {
    function FormPage(navCtrl, http) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.english = '';
        this.filipino = '';
        this.consonant = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'];
        this.vowel = ['a', 'e', 'i', 'o', 'u'];
        this.engstatus = false;
        this.filstatus = false;
        this.baystatus = false;
        this.navController = navCtrl;
        // this.translationservice = translationservice;
    }
    FormPage.prototype.status = function (data) {
        if (data == 'filipino') {
            this.englishText = '';
            this.baybayinText = '';
            this.engstatus = false;
            this.filstatus = true;
            this.baystatus = false;
        }
        else if (data == 'english') {
            this.filipinoText = '';
            this.baybayinText = '';
            this.engstatus = true;
            this.filstatus = false;
            this.baystatus = false;
        }
        else if (data == 'baybayin') {
            console.log(this.baybayinText);
            this.raw = this.baybayinText;
            this.reserve = this.raw;
            // this.baybayinText='';
            // this.processBaybay();
            this.filipinoText = '';
            this.englishText = '';
            this.engstatus = false;
            this.filstatus = false;
            this.baystatus = true;
        }
    };
    FormPage.prototype.translate = function () {
        var _this = this;
        this.filipino = '';
        this.english = '';
        if (this.engstatus) {
            this.http.get("https://translate.googleapis.com/translate_a/single?client=gtx&sl=en&tl=tl&dt=t&q=" + this.englishText)
                .map(function (res) { return res.json(); }).subscribe(function (data) {
                console.log(data);
                console.log(data[0].length);
                for (var index = 0; index < data[0].length; index++) {
                    _this.filipino = _this.filipino + data[0][index][0];
                }
                _this.filipinoText = _this.filipino;
                _this.raw = _this.filipinoText;
                _this.processBaybay();
            });
        }
        else if (this.filstatus) {
            this.raw = this.filipinoText;
            this.http.get("https://translate.googleapis.com/translate_a/single?client=gtx&sl=tl&tl=en&dt=t&q=" + this.raw)
                .map(function (res) { return res.json(); }).subscribe(function (data) {
                console.log(data);
                for (var index = 0; index < data[0].length; index++) {
                    _this.english = _this.english + data[0][index][0];
                }
                _this.englishText = _this.english;
            });
            this.processBaybay();
        }
        else if (this.baystatus) {
            this.raw = this.reserve;
            this.filipinoText = this.raw;
            this.http.get("https://translate.googleapis.com/translate_a/single?client=gtx&sl=tl&tl=en&dt=t&q=" + this.raw)
                .map(function (res) { return res.json(); }).subscribe(function (data) {
                console.log(data);
                console.log(data[0].length);
                for (var index = 0; index < data[0].length; index++) {
                    _this.english = _this.english + data[0][index][0];
                }
                _this.englishText = _this.english;
            });
            this.processBaybay();
        }
    };
    FormPage.prototype.processBaybay = function () {
        var _this = this;
        this.baybayinText = '';
        this.raw = this.raw.toLowerCase();
        this.words = this.raw.split(" ");
        console.log(this.words);
        this.words.forEach(function (element) {
            var currentstring = element.length;
            if (element.length > 2) {
                for (var index = element.length - 2; index > 0; index--) {
                    console.log(element, index);
                    if (element.charAt(index + 1) == '=' || element.charAt(index) == '=') {
                        continue;
                    }
                    if (_this.vowelbool(element.charAt(index)) == false && _this.vowelbool(element.charAt(index + 1)) == true) {
                        console.log(element.charAt(index), element.charAt(index + 1));
                        continue;
                    }
                    if (_this.vowelbool(element.charAt(index)) == true && _this.vowelbool(element.charAt(index - 1)) == true) {
                        element = _this.setCharAt(element, index, element.charAt(index).toUpperCase());
                        continue;
                    }
                    if (_this.vowelbool(element.charAt(index)) == false && _this.vowelbool(element.charAt(index - 1)) == false && _this.vowelbool(element.charAt(index + 1)) == false && element.search('ng') < 0) {
                        console.log(element.charAt(index), element.charAt(index + 1));
                        element = _this.setCharAt(element, index, element.charAt(index) + '=');
                        continue;
                    }
                    else if (_this.vowelbool(element.charAt(index)) == false && _this.vowelbool(element.charAt(index - 1)) == true && _this.vowelbool(element.charAt(index + 1)) == false && element.search('ng') < 0) {
                        console.log(element.charAt(index), element.charAt(index + 1));
                        element = _this.setCharAt(element, index, element.charAt(index) + '=');
                        continue;
                    }
                }
                console.log(element);
            }
            currentstring = element.length;
            if (_this.vowelbool(element.charAt(currentstring - 1)) == false && element.charAt(currentstring - 1) != '=') {
                element = _this.setCharAt(element, currentstring - 1, element.charAt(currentstring - 1) + '=');
            }
            if (_this.vowelbool(element.charAt(currentstring - 1)) == true && _this.vowelbool(element.charAt(currentstring - 2)) == true) {
                element = _this.setCharAt(element, currentstring - 1, element.charAt(currentstring - 1).toUpperCase());
            }
            if (_this.vowelbool(element.charAt(0))) {
                element = _this.setCharAt(element, 0, element.charAt(0).toUpperCase());
            }
            if (_this.vowelbool(element.charAt(0)) == false && _this.vowelbool(element.charAt(1)) == false && (element.charAt(1) != 'n' && element.charAt(1) != 'g')) {
                console.log(element.charAt(0), element.charAt(1));
                element = _this.setCharAt(element, 0, element.charAt(0) + '=');
            }
            var element = element.replace('ng', 'N');
            console.log(element);
            _this.baybayinText = _this.baybayinText + element + ' ';
        });
        console.log(this.baybayinText);
    };
    FormPage.prototype.setCharAt = function (str, index, chr) {
        if (index > str.length - 1)
            return str;
        return str.substr(0, index) + chr + str.substr(index + 1);
    };
    FormPage.prototype.vowelbool = function (char) {
        return /^[aeiou=]$/.test(char.toLowerCase());
    };
    FormPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-form',template:/*ion-inline-start:"/home/roneil/Desktop/alibator/camscanner-seed/src/pages/form/form.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      IsalinBayin\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="translation" padding>\n    <ion-card [ngStyle]="{ \'background-color\': \'#488AFF\' }" *ngIf="baybayinText">\n        <ion-card-header>\n          <div\n          [ngStyle]="{\n            \'background-color\': \'#488AFF\',\n            color: \'#ffff99\',\n            \'font-	size\': \'14px\',\n            \'font-weight\': \'bold\',\n            float: \'left\'\n          }"\n          >\n            Baybayin\n          </div>\n        </ion-card-header>\n        <ion-card-content>\n          <ion-list>\n            <ion-item>\n              <ion-textarea id="baybayin" rows="4" [(ngModel)]="baybayinText" disabled> </ion-textarea>\n            </ion-item>\n          </ion-list>\n        </ion-card-content>\n      </ion-card>\n  <ion-card [ngStyle]="{ \'background-color\': \'#488AFF\' }">\n    <ion-card-header>\n      <div\n        [ngStyle]="{\n          \'background-color\': \'#488AFF\',\n          color: \'#ffff99\',\n          \'font-size\': \'14px\',\n          \'font-weight\': \'bold\',\n          float: \'left\'\n        }"\n      >\n        Filipino\n      </div>\n    </ion-card-header>\n    <ion-card-content>\n      <ion-list>\n        <ion-item>\n          <ion-textarea rows="6" [(ngModel)]="filipinoText" (keydown)="status(\'filipino\')"> </ion-textarea>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n \n  <ion-card [ngStyle]="{ \'background-color\': \'#488AFF\' }">\n    <ion-card-header>\n      <div\n        [ngStyle]="{\n          \'background-color\': \'#488AFF\',\n          color: \'#ffff99\',\n          \'font-	size\': \'14px\',\n          \'font-weight\': \'bold\',\n          float: \'left\'\n        }"\n      >\n        English\n      </div>\n    </ion-card-header>\n    <ion-card-content>\n      <ion-list>\n        <ion-item>\n          <ion-textarea rows="6" [(ngModel)]="englishText" (keydown)="status(\'english\')">\n           \n          </ion-textarea>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n<ion-card>\n  <button  ion-button large full (click)="translate();">Translate</button>\n</ion-card>\n</ion-content>\n'/*ion-inline-end:"/home/roneil/Desktop/alibator/camscanner-seed/src/pages/form/form.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */]])
    ], FormPage);
    return FormPage;
}());

//# sourceMappingURL=form.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/roneil/Desktop/alibator/camscanner-seed/src/pages/home/home.html"*/'\n\n<ion-content padding>\n\n  \n  <ion-grid>\n      <ion-row>\n          <ion-col col-3>\n              <ion-avatar>\n                  <img src="../assets/imgs/icon.png"> \n            </ion-avatar>\n      </ion-col>\n        <ion-col col-9>\n          <div style="    padding-left: 15px;\n          ">\n          <h1>ISALINBAYIN</h1>\n          <h2 id="baybayin">Isalinbayin</h2>\n        </div>\n       </ion-col>\n      \n     \n      </ion-row>\n    </ion-grid>\n\n  <p>Baybayin is an ancient script used primarily by the Tagalog people. The term "baybayin" literally means "to spell, write, and syllabize" in Tagalog. \n  </p><p>\n   This educational application is for those people who would want to get oriented with the Baybayin script by translating english and filipino words into baybayin characters. The mobile application can handle simple and basic Filipino and English words input and translate it for transcribing into Baybayin characters. \n  </p>\n  \n</ion-content>\n'/*ion-inline-end:"/home/roneil/Desktop/alibator/camscanner-seed/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KeyboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var KeyboardPage = /** @class */ (function () {
    function KeyboardPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.filipino = '';
        this.baybayin = '';
        this.alphabet = [
            { title: 'A', sub: 'a', class: '' },
            { title: 'E', sub: 'e', class: '' },
            { title: 'I', sub: 'i', class: '' },
            { title: 'O', sub: 'o', class: '' },
            { title: 'U', sub: 'u', class: '' },
            { title: 'b=', sub: 'b', class: '' },
            { title: 'k=', sub: 'k', class: '' },
            { title: 'd=', sub: 'd', class: '' },
            { title: 'g=', sub: 'g', class: '' },
            { title: 'h=', sub: 'h', class: '' },
            { title: 'l=', sub: 'l', class: '' },
            { title: 'm=', sub: 'm', class: '' },
            { title: 'n=', sub: 'n', class: 'lowercase' },
            { title: 'N=', sub: 'ng', class: '' },
            { title: 'r=', sub: 'r', class: '' },
            { title: 'p=', sub: 'p', class: '' },
            { title: 's=', sub: 's', class: '' },
            { title: 't=', sub: 't', class: '' },
            { title: 'w=', sub: 'w', class: '' },
            { title: 'y=', sub: 'y', class: '' },
        ];
    }
    KeyboardPage.prototype.press = function (data) {
        this.filipino = this.filipino + data.sub;
        // this.baybayin = this.baybayin + data.title;
        if (this.vowelbool(data.sub) && this.baybayin.length > 0 && this.baybayin.charAt(this.baybayin.length - 1) == '=') {
            // this.setCharAt(this.baybayin, this.baybayin.length-1,'');
            this.baybayin = this.baybayin.substring(0, this.baybayin.length - 1);
            this.baybayin = this.baybayin + data.title.toLowerCase();
            // this.baybayin = this.baybayin.substring(0, this.baybayin.length - 1);
        }
        else {
            this.baybayin = this.baybayin + data.title;
        }
        console.log(this.baybayin);
    };
    KeyboardPage.prototype.remove = function () {
        if (this.filipino.length == 0 || this.filipino.length == 1) {
            this.filipino = '';
            this.baybayin = '';
        }
        else if (this.filipino.charAt(this.filipino.length - 1) == 'g' && this.filipino.charAt(this.filipino.length - 2) == 'n') {
            this.baybayin = this.baybayin.substring(0, this.baybayin.length - 1);
            this.baybayin = this.baybayin.substring(0, this.baybayin.length - 1);
            this.filipino = this.filipino.substring(0, this.filipino.length - 1);
            this.filipino = this.filipino.substring(0, this.filipino.length - 1);
        }
        else if (this.filipino.charAt(this.filipino.length - 1) == ' ') {
            this.baybayin = this.baybayin.substring(0, this.baybayin.length - 1);
            this.filipino = this.filipino.substring(0, this.filipino.length - 1);
        }
        else if (this.vowelbool(this.filipino.charAt(this.filipino.length - 1))) {
            this.filipino = this.filipino.substring(0, this.filipino.length - 1);
            this.baybayin = this.baybayin.substring(0, this.baybayin.length - 1);
            if (this.vowelbool(this.filipino.charAt(this.filipino.length - 1))) {
                this.baybayin.charAt(this.filipino.length - 1).toUpperCase();
            }
            else if (this.filipino.charAt(this.filipino.length - 1) == ' ' || this.filipino.charAt(this.filipino.length - 1) == '=') {
            }
            else if (this.vowelbool(this.filipino.charAt(this.filipino.length - 1)) == false) {
                this.baybayin = this.baybayin + '=';
            }
        }
        else if (this.vowelbool(this.filipino.charAt(this.filipino.length - 1)) == false) {
            this.baybayin = this.baybayin.substring(0, this.baybayin.length - 1);
            this.baybayin = this.baybayin.substring(0, this.baybayin.length - 1);
            this.filipino = this.filipino.substring(0, this.filipino.length - 1);
            console.log(this.baybayin);
            if (this.vowelbool(this.filipino.charAt(this.filipino.length - 1))) {
                this.baybayin.charAt(this.filipino.length - 1).toUpperCase();
            }
        }
        console.log(this.baybayin);
        console.log(this.filipino);
    };
    KeyboardPage.prototype.reset = function () {
        this.filipino = '';
        this.baybayin = '';
    };
    KeyboardPage.prototype.space = function () {
        this.filipino = this.filipino + ' ';
        this.baybayin = this.baybayin + ' ';
    };
    KeyboardPage.prototype.setCharAt = function (str, index, chr) {
        if (index > str.length - 1)
            return str;
        return str.substr(0, index) + chr + str.substr(index + 1);
    };
    KeyboardPage.prototype.vowelbool = function (char) {
        return /^[aeiou=]$/.test(char.toLowerCase());
    };
    KeyboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-keyboard',template:/*ion-inline-start:"/home/roneil/Desktop/alibator/camscanner-seed/src/pages/keyboard/keyboard.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>IsalinBayin</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-card [ngStyle]="{ \'background-color\': \'#488AFF\' }">\n    <ion-card-header>\n      <div [ngStyle]="{\n              \'background-color\': \'#488AFF\',\n              color: \'#ffff99\',\n              \'font-size\': \'14px\',\n              \'font-weight\': \'bold\',\n              float: \'left\'\n            }">\n        Filipino\n      </div>\n    </ion-card-header>\n    <ion-card-content>\n      <ion-list>\n        <ion-item>\n          <ion-textarea rows="6" [(ngModel)]="filipino" disabled> </ion-textarea>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n  <ion-card [ngStyle]="{ \'background-color\': \'#488AFF\' }">\n    <ion-card-header>\n      <div [ngStyle]="{\n            \'background-color\': \'#488AFF\',\n            color: \'#ffff99\',\n            \'font-	size\': \'14px\',\n            \'font-weight\': \'bold\',\n            float: \'left\'\n          }">\n        Baybayin\n      </div>\n    </ion-card-header>\n    <ion-card-content>\n      <ion-list>\n        <ion-item>\n          <ion-textarea id="baybayin" rows="3" [(ngModel)]="baybayin" disabled> </ion-textarea>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n  <ion-grid>\n    <ion-row>\n      <ion-col *ngFor="let item of alphabet">\n        <button large ion-button (click)="press(item);"> <small>{{item.sub}}</small>&nbsp;\n          <h1 class="{{item.class}} baybayin">{{item.title}}</h1>\n        </button>\n      </ion-col>\n    </ion-row>\n\n\n  </ion-grid>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6>\n          <button ion-button large style="width: 96%;" (click)="space();" >\n            space\n          </button>\n     </ion-col>\n      <ion-col col-3> <button ion-button large  style="width: 96%;" (click)="remove();" >\n          <ion-icon name="backspace"></ion-icon>\n        </button></ion-col>\n      <ion-col col-3> <button ion-button large style="width: 96%;"  (click)="reset();" >\n          clear\n        </button></ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/home/roneil/Desktop/alibator/camscanner-seed/src/pages/keyboard/keyboard.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], KeyboardPage);
    return KeyboardPage;
}());

//# sourceMappingURL=keyboard.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(233);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 233:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_progressbar_core__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_scan_scan__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_form_form__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_home_home__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_keyboard_keyboard__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_status_bar__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_splash_screen__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_common_http__ = __webpack_require__(304);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_http__ = __webpack_require__(106);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













// import {TranslationService}  from './providers/translation.service';


var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_scan_scan__["a" /* ScanPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_form_form__["a" /* FormPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_keyboard_keyboard__["a" /* KeyboardPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_14__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_common_http__["a" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_5__ngx_progressbar_core__["b" /* NgProgressModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_scan_scan__["a" /* ScanPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_form_form__["a" /* FormPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_keyboard_keyboard__["a" /* KeyboardPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 274:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/roneil/Desktop/alibator/camscanner-seed/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/roneil/Desktop/alibator/camscanner-seed/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[213]);
//# sourceMappingURL=main.js.map